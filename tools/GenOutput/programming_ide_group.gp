# Generated with Skilled Round The World Gnuplot Script Generator 
set timestamp "Last update %Y-%m-%d, %T%z, http://skilledroundtheworld.net" top rotate
set encoding utf8
set key above title "Legend"
#set term pdf
set term canvas size 800, 600
set xtics rotate by -45
set ytics
set mytics 10
set grid ytics mytics
set boxwidth 0.8
set ylabel "No of proffesionals"
set xlabel "Time [days]"
set title "Number of proffesionals with specified skills"
set output "output_graphs/programming_ide_group.html"
plot "out/size_dev_c++" u 3:xtic(1) t "DEV_C++" with linespoints, "out/size_codeblocks" u 3:xtic(1) t "CODEBLOCKS" with linespoints, "out/size_eclipse" u 3:xtic(1) t "ECLIPSE" with linespoints, "out/size_netbeans" u 3:xtic(1) t "NETBEANS" with linespoints, "out/size_source_insight" u 3:xtic(1) t "SOURCE_INSIGHT" with linespoints, "out/size_emacs" u 3:xtic(1) t "EMACS" with linespoints, "out/size_kdevelop" u 3:xtic(1) t "KDEVELOP" with linespoints, "out/size_microsoft_visual_studio_c++" u 3:xtic(1) t "MICROSOFT_VISUAL_STUDIO_C++" with linespoints, "out/size_borland_c++_builder" u 3:xtic(1) t "BORLAND_C++_BUILDER" with linespoints, "out/size_kylix" u 3:xtic(1) t "KYLIX" with linespoints, "out/size_c++_builder" u 3:xtic(1) t "C++_BUILDER" with linespoints, "out/size_borland_delphi" u 3:xtic(1) t "BORLAND_DELPHI" with linespoints, "out/size_delphi_6" u 3:xtic(1) t "DELPHI_6" with linespoints, 