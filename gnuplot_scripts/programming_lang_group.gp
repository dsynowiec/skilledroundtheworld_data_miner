set timestamp "Last update %Y-%m-%d, %T%z, http://skilledroundtheworld.net" top rotate
set encoding utf8
set key above title "Legend"
#set term pdf
set term canvas size 800, 600
set xtics rotate by -45
set ytics
set mytics 10
set grid ytics mytics
set boxwidth 0.8
set ylabel "No of proffesionals"
set xlabel "Time [days]"
set output "output_graphs/programming_languages_group.html"
set title "Number of proffesionals with specified skills"

plot "out/size_c" u 3:xtic(1) t "C" with linespoints, "out/size_c++" u 3:xtic(1) t "C++" with linespoints, "out/size_c++_cli" u 3:xtic(1) t "C++/CLI" with linespoints, "out/size_embedded_c" u 3:xtic(1) t "Embedded C" with linespoints, "out/size_java" u 3:xtic(1) t "Java" with linespoints, "out/size_python" u 3:xtic(1) t "Python" with linespoints, "out/size_qt" u 3:xtic(1) t "QT" with linespoints, "out/size_ruby" u 3:xtic(1) t "Ruby" with linespoints, "out/size_perl" u 3:xtic(1) t "Perl" with linespoints, "out/size_fortran" u 3:xtic(1) t "Fortran" with linespoints, "out/size_algol" u 3:xtic(1) t "Algol" with linespoints, "out/size_objective-c" u 3:xtic(1) t "Objective C" with linespoints, "out/size_c#" u 3:xtic(1) t "C#" with linespoints, "out/size_php" u 3:xtic(1) t "PHP" with linespoints, "out/size_visual_basic" u 3:xtic(1) t "Visual Basic" with linespoints, "out/size_javascript" u 3:xtic(1) t "JavaScript" with linespoints, "out/size_sql" u 3:xtic(1) t "SQL" with linespoints, "out/size_lisp" u 3:xtic(1) t "Lisp" with linespoints, "out/size_pascal" u 3:xtic(1) t "Pascal" with linespoints

