set timestamp "Last update %Y-%m-%d, %T%z, http://skilledroundtheworld.net" top rotate
set encoding utf8
set key above title "Legend"
#set term pdf
set term canvas size 800, 600
set xtics rotate by -45
set ytics
set mytics 10
set grid ytics mytics
set boxwidth 0.8
set ylabel "No of proffesionals"
set xlabel "Time [days]"
set output "output_graphs/programming_IDEs_group.html"
set title "Number of proffesionals with specified skills"

plot "out/size_dev_c++" u 3:xtic(1) t "Dev C++" with linespoints, "out/size_codeblocks" u 3:xtic(1) t "Code::Blocks" with linespoints, "out/size_eclipse" u 3:xtic(1) t "Eclipse" with linespoints, "out/size_netbeans" u 3:xtic(1) t "NetBeans" with linespoints, "out/size_source_insight" u 3:xtic(1) t "Source Insight" with linespoints, "out/size_emacs" u 3:xtic(1) t "Emacs" with linespoints, "out/size_kdevelop" u 3:xtic(1) t "KDevelop" with linespoints, "out/size_microsoft_visual_studio_c++" u 3:xtic(1) t "MSVS" with linespoints, "out/size_borland_c++_builder" u 3:xtic(1) t "Borland C++ Builder" with linespoints, "out/size_kylix" u 3:xtic(1) t "Kylix" with linespoints, "out/size_c++_builder" u 3:xtic(1) t "C++ Builder" with linespoints, "out/size_borland_delphi" u 3:xtic(1) t "Borland Delphi" with linespoints, "out/size_delphi_6" u 3:xtic(1) t "Delphi 6" with linespoints

